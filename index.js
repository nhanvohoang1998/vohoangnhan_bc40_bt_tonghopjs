// Bài tập 1:
let k = 1
let sumAllContent = ""
for (let i = 0; i < 10; i++) {
    let content = ""
    for (let j = 0; j < 10; j++) {
        let contentTr = `
                        <td>${k}</td>
                        `
        content += contentTr;
        k++;
    }
    // console.log(content)
    sumAllContent += `<tr>${content}</tr>`
}
document.getElementById("table1").innerHTML = sumAllContent

// Bài tập 2:
let mang = [];
let themGiaTriVaoMang = () => {
    mang.push(document.getElementById("nhapSoN").value * 1);
    document.getElementById("showMang").innerHTML = `Mảng hiện tại: [${mang}]`
}

let kiemTraSoNguyenTo = (item) => {
    let isSoNguyenTo = true;
    if (item == 2 || item == 3) {
        isSoNguyenTo = true
    } else if (item > 3) {
        for (let i = 2; i <= Math.sqrt(item); i++) {
            if (item % i == 0) {
                isSoNguyenTo = false
                break;
            }
        }
    } else {
        isSoNguyenTo = false
    }
    return isSoNguyenTo
}
let showKetQuaSoNguyenTo = () => {
    let mangSoNguyenTo = []
    mang.forEach((item) => {
        if (kiemTraSoNguyenTo(item)) {
            mangSoNguyenTo.push(item)
        }
    })
    let content = ``
    mangSoNguyenTo.forEach((item) => {
        content += item + ", "
    })
    document.getElementById("ketQuaBT2").innerHTML = `Kết quả: ${content}`
}
document.getElementById("themVaoMang").addEventListener("click", themGiaTriVaoMang)
document.getElementById("timSoNguyenTo").addEventListener("click", showKetQuaSoNguyenTo)

//Bài tập 3:
let tinhTong = () => {
    let nhapSoN2 = document.getElementById("nhapSoN2").value * 1;
    let tong = null
    for (let i = 2; i <= nhapSoN2; i++) {
        tong += i;
    }
    tong += 2 * nhapSoN2
    document.getElementById("ketQuaBT3").innerHTML = `Kết quả: ${tong}`
}
document.getElementById("tinhTong").addEventListener("click", tinhTong)

//Bài tập 4:
let tinhSoLuongUoc = () => {
    const nhapSoN3 = document.getElementById('nhapSoN3').value * 1;
    let mangUocSo = []
    for (let i = 1; i <= nhapSoN3; i++) {
        if (nhapSoN3 % i == 0) {
            mangUocSo.push(i);
        }
    }
    document.getElementById("ketQuaBT4").innerHTML = `Kết quả: [${mangUocSo}]`
}
document.getElementById("tinhSoLuongUoc").addEventListener("click", tinhSoLuongUoc)

//Bài tập 5:
let timSoDaoNguoc = () => {
    const nhapSoN4 = document.getElementById('nhapSoN4').value;
    let mangChuaChuoi = []
    for (let i = 0; i < nhapSoN4.length; i++) {
        mangChuaChuoi.push(nhapSoN4[i]);
    }

    document.getElementById("ketQuaBT5").innerHTML = `Kết quả: ${mangChuaChuoi.reverse().join("")}`
}
document.getElementById("timSoDaoNguoc").addEventListener("click", timSoDaoNguoc)

//Bài tập 6:
let timX = () => {
    let tong = 0;
    for (let i = 1; i < 100; i++) {
        tong += i;
        if (tong >= 100) {
            document.getElementById("ketQuaBT6").innerHTML = `Kết quả: x = ${i - 1}`;
            break
        }
    }
}
timX()

//Bài tập 7:
let inBangCuuChuong = () => {
    const nhapSoN5 = document.getElementById('nhapSoN5').value * 1;
    document.getElementById("ketQuaBT7").innerHTML = `
                                                    Bảng cửu chương ${nhapSoN5} <br />
                                                    ${nhapSoN5} x 0 = 0 <br />
                                                    ${nhapSoN5} x 1 = ${nhapSoN5 * 1} <br />
                                                    ${nhapSoN5} x 2 = ${nhapSoN5 * 2} <br />
                                                    ${nhapSoN5} x 3 = ${nhapSoN5 * 3} <br />
                                                    ${nhapSoN5} x 4 = ${nhapSoN5 * 4} <br />
                                                    ${nhapSoN5} x 5 = ${nhapSoN5 * 5} <br />
                                                    ${nhapSoN5} x 6 = ${nhapSoN5 * 6} <br />
                                                    ${nhapSoN5} x 7 = ${nhapSoN5 * 7} <br />
                                                    ${nhapSoN5} x 8 = ${nhapSoN5 * 8} <br />
                                                    ${nhapSoN5} x 9 = ${nhapSoN5 * 9} <br />
                                                    ${nhapSoN5} x 10 = ${nhapSoN5 * 10} <br />
                                                    `
}
document.getElementById("inBangCuuChuong").addEventListener("click", inBangCuuChuong)

//Bài tập 8:
let chiaBai = () => {
    let cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S",
        "AS", "7H", "9K", "10D"]
    let player1 = []
    let player2 = []
    let player3 = []
    let player4 = []
    for (let i = 0; i < cards.length;) {
        player1.push(cards[i])
        player2.push(cards[i + 1])
        player3.push(cards[i + 2])
        player4.push(cards[i + 3])
        i = i + 4;
        document.getElementById("ketQuaBT8").innerHTML = `
                                                    Kết quả sau khi chia bài: <br />
                                                    Player 1: [${player1}] <br />
                                                    Player 2: [${player2}] <br />
                                                    Player 3: [${player3}] <br />
                                                    Player 4: [${player4}] <br />
                                                    `
    }
}
document.getElementById("chiaBai").addEventListener("click", chiaBai)

//Bài tập 9:
let timSoGaVaCho = ()=>{
    const nhapSoM = document.getElementById('nhapSoM').value*1;
    const nhapSoN6 = document.getElementById('nhapSoN6').value*1;
    let cho = nhapSoN6/2 - nhapSoM;
    let ga = nhapSoM - cho;
    document.getElementById("ketQuaBT9").innerText= `Kết quả: ${ga} con gà và ${cho} con chó
                                                    `
}
document.getElementById("timSoGaVaCho").addEventListener("click",timSoGaVaCho)

//Bài tập 10:
/**
 * 1 phút : 360/60 = 6 độ
 * 1 giờ kim giờ quay được 360/12 = 30 độ
 * 1 phút kim giờ quay được  30/60 = 0.5 độ
 */
let tinhGocLech =()=>{
    const nhapSoGio = document.getElementById('nhapSoGio').value*1;
    const nhapSoPhut = document.getElementById('nhapSoPhut').value*1;
    let gocLech = Math.abs(nhapSoPhut*6 - 0.5*(nhapSoGio*60+nhapSoPhut))
    document.getElementById("ketQuaBT10").innerHTML=`Kết quả: ${gocLech} độ`
}
document.getElementById("tinhGocLech").addEventListener("click",tinhGocLech)